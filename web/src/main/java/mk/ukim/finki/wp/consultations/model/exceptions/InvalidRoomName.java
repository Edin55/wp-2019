package mk.ukim.finki.wp.consultations.model.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class InvalidRoomName extends RuntimeException {

    public InvalidRoomName() {
        super(InvalidRoomName.class.getSimpleName());
    }
}
